package net.way2java.demo.rabbitmq.consumer.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

@Slf4j
public class JangbMessageListener implements MessageListener {
    @Override
    public void onMessage(Message message) {
        log.debug("message = [ {} ]", new String(message.getBody()));
    }
}
