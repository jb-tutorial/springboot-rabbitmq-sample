package net.way2java.demo.rabbitmq.consumer.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.MessageListenerContainer;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {
    private static final String secondJangbQueue = "secondJangbQueue";

    @Bean
    Queue secondJangbQueue() {
        return new Queue(secondJangbQueue, true);
    }

    @Bean
    Exchange secondJangbExchange() {
        return ExchangeBuilder
                .topicExchange("secondJangbExchange")
                .durable(true)
                .build();
    }

    @Bean
    Binding binding(Queue secondJangbQueue, Exchange secondJangbExchange){
        return BindingBuilder
                .bind(secondJangbQueue)
                .to(secondJangbExchange)
                .with("topic")
                .noargs();
    }

    @Bean
    ConnectionFactory connectionFactory() {
        var cachingConnectionFactory = new CachingConnectionFactory("192.168.52.32");
        cachingConnectionFactory.setUsername("jangb");
        cachingConnectionFactory.setPassword("jangb");

        return cachingConnectionFactory;
    }

    @Bean
    MessageListenerContainer messageListenerContainer(ConnectionFactory connectionFactory, Queue secondJangbQueue) {
        var messageListenerContainer = new SimpleMessageListenerContainer();
        messageListenerContainer.setConnectionFactory(connectionFactory);
        messageListenerContainer.setQueues(secondJangbQueue);
        messageListenerContainer.setMessageListener(new JangbMessageListener());

        return messageListenerContainer;
    }
}
